resource "aws_subnet" "my_subnet_ip1" {
  vpc_id            = aws_vpc.paginator_vpc.id
  cidr_block        = "10.0.4.0/24"
  availability_zone = "eu-west-2a"


}
resource "aws_subnet" "my_subnet_ip2" {
  vpc_id            = aws_vpc.paginator_vpc.id
  cidr_block        = "10.0.2.0/24"
  availability_zone = "eu-west-2b"


}
resource "aws_subnet" "my_subnet_ip3" {
  vpc_id            = aws_vpc.paginator_vpc.id
  cidr_block        = "10.0.3.0/24"
  availability_zone = "eu-west-2c"
}