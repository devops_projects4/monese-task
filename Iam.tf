resource "aws_iam_role" "paginator_role" {
  name = "paginator_iam_role"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
EOF

}

resource "aws_iam_policy" "paginator_policy" {
  name        = "paginator-policy"
  description = "A policy that gives permission to the paginator role "

  policy = <<EOF
{
  "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : "logs:CreateLogGroup",
          "Resource" : "arn:aws:logs:eu-west-2:029764709254:*"
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "logs:CreateLogStream",
            "logs:PutLogEvents"
          ],
          "Resource" : [
            "arn:aws:logs:eu-west-2:029764709254:log-group:/aws/lambda/monese-test-role:*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:GetObject",
            "s3:ListBucket"
          ],
          "Resource" : "arn:aws:s3:::*"
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:*",
            "s3-object-lambda:*"
          ],
          "Resource" : "*"
        }
      ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "paginator_attachment" {
  role       = aws_iam_role.paginator_role.name
  policy_arn = aws_iam_policy.paginator_policy.arn
}
