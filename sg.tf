resource "aws_security_group" "paginator_security_group" {
  name        = "paginator_security_group"
  description = "Allow inbound traffic"
  vpc_id      = aws_vpc.paginator_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {

    Name = "my-security-group"
    Environment = var.enviroment
  }
}
