import boto3
import os
import logging
from botocore.exceptions import ClientError
s3_client = boto3.client('s3')
numberOfFiles = 1050 #set the number of s3 object

def upload_file():

    # Upload the file
    s3_client = boto3.client('s3')
    try:
      for n in range(numberOfFiles):
        filename = "test" + str(n) + ".txt"
        with open(filename, 'w+b'): # create the file
        #send the file to s3 bucket
         response = s3_client.upload_file(filename,"monese-s3-bucket-135", filename)
    except ClientError as e:
        logging.error(e)
        return False
    return True

upload_file()