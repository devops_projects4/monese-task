variable "enviroment" {
    type = string
    description = "Targeted AWS Environment"
    default = "Dev"
}

variable "paginator_elb" {
    type = string
    description = "Name of ALB"
    default = "paginator-elb"
}

variable "service_name" {
    type = string
    description = "Name of application service"
    default = "paginator"
}
variable "bucket_name" {
    type = string
    description = "Name of the s3 bucket"
    default = "monese-s3-bucket-135"
  
}
variable "bucket_log_name" {
    type = string
    description = "name of log bucket"
    default = "monese-log-bucket-135"
}
variable "vpc_cidr_block" {
    type = string
    description = "vpc cidr block"
    default = "10.0.0.0/16"
}

variable "region" {
    type = string
    description = "aws region to create infrastructure"
    default = "eu-west-2"
}