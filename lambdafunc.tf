resource "aws_lambda_function" "paginator_lambda_function" {
  filename      = "generateobjects.zip"
  function_name = "s3objectpaginator"
  role          = aws_iam_role.paginator_role.arn
  handler       = "generateobjects.lambda_handler"
  source_code_hash = filebase64sha256("generateobjects.zip")
  runtime = "python3.9"
  timeout = 60

  environment {
    variables = {
      bucket = var.bucket_name
      region = var.region
    }
  }

  provisioner "local-exec" {
    command = "python3 s3objectgenerator.py"
  }

}
