resource "aws_lb" "paginator_elb" {
  name               = var.paginator_elb
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.paginator_security_group.id]
  subnets            = [aws_subnet.my_subnet_ip1.id, aws_subnet.my_subnet_ip2.id, aws_subnet.my_subnet_ip3.id]
  enable_deletion_protection = false

  tags = {

    "Environment" = var.enviroment
  }
}

resource "aws_lb_target_group" "paginator_target_group" {
  name        = "paginator-tg"
  target_type = "lambda"

   health_check {
    path                = "/"
    port                = 80
    protocol            = "HTTP"
    healthy_threshold   = 5
    unhealthy_threshold = 3
    timeout = 30
    interval = 35
    matcher             = "200"
  }
}

resource "aws_lb_target_group_attachment" "paginator_group_attachment" {
  target_group_arn = aws_lb_target_group.paginator_target_group.arn
  target_id        = aws_lambda_function.paginator_lambda_function.arn
  depends_on       = [aws_lambda_permission.paginator_for_lb]
}

resource "aws_lb_listener" "paginator_listener" {
  load_balancer_arn = aws_lb.paginator_elb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.paginator_target_group.arn
  }
}

resource "aws_lambda_permission" "paginator_for_lb" {
  statement_id  = "AllowExecutionFromlb"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.paginator_lambda_function.arn
  principal     = "elasticloadbalancing.amazonaws.com"
  source_arn    = aws_lb_target_group.paginator_target_group.arn

}
resource "aws_lb_listener_rule" "paginator_listener_rule" {
  listener_arn = aws_lb_listener.paginator_listener.arn
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.paginator_target_group.arn
  }

  condition {
    path_pattern {
      values = ["/lambda/get/objects"]
    }
  }
}

