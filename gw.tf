
#creates a VPC network with internet gateway
resource "aws_internet_gateway" "paginator_igw" {
  vpc_id = aws_vpc.paginator_vpc.id

  tags = {

    Name = "paginator_igw"
    Environment = var.enviroment
  }
}