resource "aws_s3_bucket_versioning" "paginator_s3_versioning" {
  bucket = aws_s3_bucket.paginator_bucket.id

  versioning_configuration {
    status = "Enabled"
  }
}
