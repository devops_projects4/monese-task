terraform {
  backend "s3" {
    bucket = "statefile135"
    key    = "s3statefile/terraform.tfstate"
    region = "eu-west-2"
    encrypt = true
  }
}
