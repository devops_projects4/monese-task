
# S3 Paginator Task
## A severless task to retrieve more than 1000 object from s3 bucket


## 🔗 Links
[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://gitlab.com/devops_projects4/monese-task.git)



## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/devops_projects4/monese-task.git
```

Go to the project directory

```bash
  Authenticate the cli to configure the environment for aws
  run command aws configure
  add the access key id
  add secret access key id
  add region (eu-west-2)
```

Install dependencies

```bash
  install python3
  pip install boto3
```
#### Method one to run the code
- using the tarball s3paginator.tar
- Alternatively git clone https://gitlab.com/devops_projects4/monese-task.git
- use any IDE but in this case I used VS Code IDE; run "code ." enter to open the files

```bash
  Run the following command from this directory
  - terraform init
  - terraform plan
  - terraform apply
```
#### Method two to run the code
- To run the pipeline in Gitlab

```bash
  
  git clone https://gitlab.com/devops_projects4/monese-task.git
    From the menu, click CI/CD ---> pipeline
    The from the top right corner click the run pipeline button
  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  Wait for a complete process  in about a minute
  >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    The apply button should be intentionally left to be manually triggered
    but in this project it was removed to complete the process automatically
```
 we can access the s3 bucket only using the http://ELB-DNS/lambda/get-objects
ELB-DNS can be retrieved from the aws application load balancer env

## Tech Stack

**Client:** boto3, python3, Terraform

**Server:** AWS, Serverless


## Usage/Examples

```python
import boto3
# A snippet upload object to s3
Bucket = "monese-s3-bucket-135"
def upload_file():

  response = s3_client.upload_file(filename,Bucket, filename)

  return response
}
```
```hcl
provider "archive" {}
#To zip the python file using terraform
data "archive_file" "zip" {
  type        = "zip"
  source_file = "generateobjects.py"
  output_path = "generateobjects.zip"
}
```


## Attributes

### Python Boto3
- There are two python files in this project
- One file is to create to run as lambda function in aws
- The code build to retrieve object more than 1000 objects

### Terraform
- Terrafrom implementthe s3 bucket creation, IAM Policies to grant access/permissions to cloudwatch, S3, application load balancer
- creat python lambda function with the python code written above
- create an IAM policy to allow permissions which Terraform needs.
- create VPC, Subnet and internet gateway
- enabled versioning to prevent accidental data loss
- Version used >>4.* from documentation
- Set persistent state for {}terraform.tfstate
```hcl
 terraform {
  backend "s3" {
    bucket         = "THE_NAME_OF_THE_STATE_BUCKET"
    key            = "some_environment/terraform.tfstate"
    region         = "eu-west-2"
    encrypt        = true
  }
}
```
- provisioned to the aws cloud


### Contraints
- The python code did not include the number of object in each page
- The code when run locally will create object in the local directory except ran in the cloud/CI/CD environment
- I understand one best practise is to implement module for reuseability
- The KMS customer master key to encrypt state bucket not used.

### Time Constraints
- Did not implement terraform module
- The documentation did not explain each attributes/command used



## Acknowledgements

 - [Awesome Resources for python Boto3 for s3 paginator](https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Paginator.ListObjectsV2)
 - [Awesome Resources for Terrafrom Load Balancer](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb)
 - [Awesome Resources for Terrafrom S3 bucket config](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket)
 - [Awesome Resources for Terrafrom VPC resources](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc)
 - [Awesome Resources for Terrafrom lambda function resources](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function)
 

## Environment Variables

To run this project, you will need to add the following environment variables to your .env file

`ACCESS KEY ID`

`SECRET ACCESS KEY ID`

`AWS REGION`



## Documentation

[Documentation](https://linktodocumentation)


## Appendix

Any additional information goes here

