
resource "aws_s3_bucket_logging" "paginator_logging_bucket" {
  bucket = aws_s3_bucket.paginator_bucket.id

  target_bucket = aws_s3_bucket.paginator_log_bucket.id
  target_prefix = "log/"
}


