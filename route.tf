
resource "aws_default_route_table" "paginator-rt" {
    default_route_table_id = aws_vpc.paginator_vpc.default_route_table_id
  

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.paginator_igw.id
  }
  tags = {

    Name = "my-route-table"
  }
}