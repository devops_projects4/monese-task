resource "aws_vpc" "paginator_vpc" {
  cidr_block       = var.vpc_cidr_block
  instance_tenancy = "default"
  enable_dns_hostnames = true

  tags = {

    Name = "paginator_vpc"
    Environment = var.enviroment
  }
}
