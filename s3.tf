resource "aws_s3_bucket" "paginator_bucket" {
  bucket        = var.bucket_name
  force_destroy = true

  tags = {

    Environment = var.enviroment
  }
}

resource "aws_s3_bucket" "paginator_log_bucket" {
  bucket = var.bucket_log_name
  force_destroy = true

  tags = {

    Environment = var.enviroment
  }
 
}
