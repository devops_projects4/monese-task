#!/bin/sh
echo -e " "
echo -e "\e[92m ----Installing Build Tools----"
echo -e " "
apk update && apk upgrade --available 
apk add --update python3 py3-pip
pip3 install boto3
python3 -V
apk --no-cache update
